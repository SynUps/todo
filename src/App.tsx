import { useState } from "react";
import "./App.css";
import { TaskType, Todolist } from "./Todolist";
import { v1 } from "uuid";

export type FilterValuesType = "all" | "completed" | "active";
type TodoListType = {
  id: string;
  title: string;
  filter: FilterValuesType;
};

function App() {
  function removeTask(id: string, todolistID: string) {
    let tasks = tasksObj[todolistID];
    let filteredTasks = tasks.filter((t) => t.id !== id);
    tasksObj[todolistID] = filteredTasks;
    setTasks({ ...tasksObj });
  }

  function addTask(title: string, todolistID: string) {
    let newTask = { id: v1(), title: title, isDone: false };
    let tasks = tasksObj[todolistID];
    let newTasks = [newTask, ...tasks];
    tasksObj[todolistID] = newTasks;
    setTasks({ ...tasksObj });
  }

  function changeStatus(taskID: string, isDone: boolean, todolistID: string) {
    let tasks = tasksObj[todolistID];
    let task = tasks.find((t) => t.id === taskID);
    if (task) {
      task.isDone = isDone;
      setTasks({ ...tasksObj });
    }
  }

  function changeFilter(value: FilterValuesType, todolistID: string) {
    let todoLIST = todolists.find((t) => t.id === todolistID);
    if (todoLIST) {
      todoLIST.filter = value;
      setTodolists([...todolists]);
    }
  }

  let todoListId1 = v1();
  let todoListId2 = v1();

  let [todolists, setTodolists] = useState<Array<TodoListType>>([
    { id: todoListId1, title: "What to learn", filter: "active" },
    { id: todoListId2, title: "What to buy", filter: "completed" },
  ]);

  let removeTodolist = (todolistID: string) => {
    let filtredTodolist = todolists.filter((t) => t.id !== todolistID);
    setTodolists(filtredTodolist);
    delete tasksObj[todolistID];
    setTasks({ ...tasksObj });
  };

  let [tasksObj, setTasks] = useState({
    [todoListId1]: [
      { id: v1(), title: "CSS", isDone: true },
      { id: v1(), title: "JS", isDone: true },
      { id: v1(), title: "React", isDone: false },
    ],
    [todoListId2]: [
      { id: v1(), title: "SASS", isDone: true },
      { id: v1(), title: "LESS", isDone: true },
      { id: v1(), title: "WebPack", isDone: false },
      { id: v1(), title: "Gulp", isDone: false },
    ],
  });

  return (
    <div className="App">
      {todolists.map((t) => {
        let tasksForTodoList = tasksObj[t.id];

        if (t.filter === "completed") {
          tasksForTodoList = tasksForTodoList.filter((t) => t.isDone === true);
        }
        if (t.filter === "active") {
          tasksForTodoList = tasksForTodoList.filter((t) => t.isDone === false);
        }

        return (
          <Todolist
            key={t.id}
            id={t.id}
            title={t.title}
            tasks={tasksForTodoList}
            removeTask={removeTask}
            changeFilter={changeFilter}
            addTask={addTask}
            changeTaskStatus={changeStatus}
            filter={t.filter}
            removeTodolist={removeTodolist}
          />
        );
      })}
    </div>
  );
}

export default App;
